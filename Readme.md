# Makefile Template

This Makefile is a template for the most Makefiles in my projects. It is based
on the Makefiles used by Gehard Lazu on YouTube in a [RabbitMQ develop
series](https://www.youtube.com/channel/UCSg9GRMGAo7euj3baJi4dOg).

The main feature of these template is, that you get a list of the available
tasks (colorized) on stdout, when you call `make` on the command line.

```
$ make
create_tables                 create the foreign tables
db                            opens a connection to the db for an interactive session
ping                          check whether the database is alive
refresh                       opens a connection to the db
```

You can find the latest version of this project on [GitLab.com](https://gitlab.com/schaary/makefile-template)
