#
# This Makefile shows all targets with a short explaination, when you type
#   `make help` or short `make` - because `help` is the default target.
#
# A target is shown up in the list if it has a comment with two ## symbols.
#
# An example:
#
# target-A: ## this target does something useful
#   foo bar baz
#
# target-B:
#   foo bar baz
#
# Target B will not be listed in the in the output, when you type `make help`.
#
# Some preparations

MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -eu pipefail -c

### TARGETS ###
#

.DEFAULT_GOAL = help

.PHONY: help
help:
	@awk -F':+ |##' '/^[^\.][0-9a-zA-Z\._\-%]+:+.+##.+$$/ { printf "\033[36m%-28s\033[0m %s\n", $$1, $$3 }' $(MAKEFILE_LIST) \
	| sort

define MAKE_TARGETS
  awk -F':+' '/^[^\.%\t\_][0-9a-zA-Z\._\-\%]*:+.*$$/ { printf "%s\n", $$1 }' $(MAKEFILE_LIST)
endef
define BASH_AUTOCOMPLETE
  complete -W \"$$($(MAKE_TARGETS) | sort | uniq)\" make gmake m
endef

# this target set the environment to auto complete on the makefile targets
.PHONY: bash-autocomplete
bash-autocomplete:
	@echo "$(BASH_AUTOCOMPLETE)"
.PHONY: bac
bac: bash-autocomplete

# example target
.PHONY: ping
ping: ## send a ping to https://google.com
	@ping -o google.com
